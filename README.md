legacyservices
=========

This role removes several legacy services and uninstalls the software in case it is installed

Requirements
------------

None

Role Variables
--------------

enable_legacyservices: true - [controls if the module will run at all]

legacyservices_remove_xinetd: true - [removes xinetd package]

legacyservices_remove_tftp: true - [removes tftp package]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
